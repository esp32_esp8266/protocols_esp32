### UART Introduction

Universal Asynchronous Receive Transmit (UART) or Serial communication is one of the most simple communication protocols between two devices. It transfers data between devices by connecting two wires between the devices, one is the transmission line while the other is the receiving line. 

The data transfers bit by bit digitally in form of bits from one device to another. The main advantage of this communication protocol is that its not necessary for both the devices to have the same operating frequency. 

For example, two microcontrollers operating at different clock frequencies can communicate with each other easily via serial communication. However, a predefined bit rate that is referred to as baud rate usually set in the flash memory of both microcontrollers for the instruction to be understood by both the devices.

## Transmitting and receiving serial data

The transmitting UART takes bytes of data and transmits the bits in a sequential form. The second transmitter which is the receiver reassembles the bits into a complete byte. Serial transmission of data through a single wire is actually more cost-effective than parallel transmission through multiple wires.

Communication between two UART devices may be simplex, full-duplex or half-duplex. Simplex communication is a one-direction type of communication where the signal moves from one UART to another. It doesn’t have provision for the receiving UART to send back signals.  A full-duplex is when both devices can transmit and receive communications at the same time. A half-duplex is when devices take turns to transmit and receive.

## Structure of a UART Protocol

A UART contains a clock generator. This allows the sampling in a bit period.
It also contains input and output shift registers.

Transmitting and receiving control.
A read or write control logic.

Other optional components of a UART are: transmit or receive buffers, FIFO buffer memory, and a DMA controller.

## UART technology 

There was a time not so long ago when keyboards, mice, and printers had thick cables and clunky connectors. These had to be literally screwed into the computer. These devices where using UART to communicate with computers. Although these clunky cables have been replaced with USB, you can still find UARTs being used in DIY electronics such as Raspberry Pi, Arduino, and other common microcontrollers. We can use it to connect Bluetooth modules and GPS modules.

UART has a different transfer protocol than other communication protocols such as SPI and I2C. It is a physical circuit fount in a microcontroller. It can also function as a stand-alone integrated circuit. One significant advantage of UART is that it only relies on two wires to transmit data.

It takes two UART’s to communicate directly with each other. On one end the transmitting UART converts parallel data from a CPU into serial form then transmits the data in serial form to the second UART which will receive the serial data and convert it back into parallel data. This data can then be accessed from the receiving device.

Instead of cloak signals the transmitting and receiving bit use start and stop bit signals for the data packages. These start and stop bits define the beginning and the end of the data packages. Therefore the receiving UART knows when to start and stop reading the bits.

The Receiving UART will detect the start bit then start reading the bits. The specific frequency used to read the incoming bits is known as the baud rate. The baud rate is a measure used for the speed of data transfer. The unit used for baud rate is bits per second (bps). In order for the data transfer to be a success both the transmitting and receiving UART must operate at almost the same baud rate. However, if the baud rates differ between both UARTs they must only differ by 10%. The receiving and transmitting UART must be configured to receive the same data packages.


### ESP32 UART Pins

ESP32 provides three universal asynchronous receivers and transmitter (UART) ports such as UART0, UART1, and UART2 that work at 3.3V TTL level. 

These three serial interfaces are hardware supported. Each of them exposes 4 pins: RX, TX, RTS and CTS. However, the Arduino IDE only uses RX and TX pins.

### SPI

Introducing ESP32 SPI Communication Protocol

SPI stands for Serial Peripheral Interface, and it is a synchronous serial data protocol used by microcontrollers to communicate with one or more peripherals. For example, your ESP32 board communicating with a sensor that supports SPI or with another microcontroller.

In an SPI communication, there is always a controller (also called master) that controls the peripheral devices (also called slaves). Data can be sent and received simultaneously. This means that the master can send data to a slave, and a slave can send data to the master at the same time.

You can have only one master, which will be a microcontroller (the ESP32), but you can have multiple slaves. A slave can be a sensor, a display, a microSD card, etc., or another microcontroller. This means you can have an ESP32 connected to multiple sensors, but the same sensor can’t be connected to multiple ESP32 boards simultaneously.


### ESP32 SPI Peripherals

The ESP32 integrates 4 SPI peripherals: SPI0, SPI1, SPI2 (commonly referred to as HSPI), and SPI3 (commonly referred to as VSPI).

SP0 and SP1 are used internally to communicate with the built-in flash memory, and you should not use them for other tasks.

You can use HSPI and VSPI to communicate with other devices. HSPI and VSPI have independent bus signals, and each bus can drive up to three SPI slaves.

Warning: depending on the board you’re using, the default SPI pins might be different. So, make sure you check the pinout for the board you’re using. Additionally, some boards don’t have pre-assigned SPI pins, so you need to set them on code.

Note: usually, when not specified, the board will use the VSPI pins when initializing an SPI communication with the default settings.

Whether your board comes with pre-assigned pins or not, you can always set them on code.
